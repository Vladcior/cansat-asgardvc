#include <Arduino.h>
#include "config.h"

// handle diagnostic informations given by assertion and abort program execution:
void __assert(const char *__func, const char *__file, int __lineno, const char *__sexp) {
    // transmit diagnostic informations through serial link. 
    Serial.print(F("**Assertion failed: "));
    Serial.println(__sexp);
    Serial.print(F("In: ")); 
    Serial.println(__func);
    Serial.print(__file);
    Serial.print(F(",line "));
    Serial.println(__lineno, DEC);
    Serial.flush();
    // abort program execution.
    abort();
}
