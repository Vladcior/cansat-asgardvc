// Test code for Adafruit GPS modules using MTK3329/MTK3339 driver
//
// This code just echos whatever is coming from the GPS unit to the
// serial monitor, handy for debugging!
//
// Based on Adafruit example, customized to use Feather M0 Express 
// hardware Serial or Serial2 based on Sercom1.
/* Connexions: 
 *    GPS TX (transmit) pin to Feather M0 Express  RX and D10
 *    GPS RX (receive) pin to Feather M0 Express  TX and D11
 *    GPS Vin to 3.3 V
 *    GPS GND to GND
 *    
 *  Define symbol USE_HARDWARE_SERIAL to use the RX-TX wires, undefine it to 
 *  use the D10-D11 wires. 
 */

//#define USE_HARDWARE_SERIAL

#ifndef ARDUINO_ARCH_SAMD
#error This test is for SAMD boards only
#endif

#include <Adafruit_GPS.h>

// Connect the GPS Power pin to 5V
// Connect the GPS Ground pin to ground

#ifdef USE_HARDWARE_SERIAL
HardwareSerial &mySerial = Serial1;
#else
// This is to use Serial2, configured on Sercom1
#include "Serial2.h"
#include "wiring_private.h"
auto &mySerial = Serial2; 
#endif

#define PMTK_SET_NMEA_UPDATE_1HZ  "$PMTK220,1000*1F"
#define PMTK_SET_NMEA_UPDATE_5HZ  "$PMTK220,200*2C"
#define PMTK_SET_NMEA_UPDATE_10HZ "$PMTK220,100*2F"

// turn on only the second sentence (GPRMC)
#define PMTK_SET_NMEA_OUTPUT_RMCONLY "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"
// turn on GPRMC and GGA
#define PMTK_SET_NMEA_OUTPUT_RMCGGA "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"
// turn on ALL THE DATA
#define PMTK_SET_NMEA_OUTPUT_ALLDATA "$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28"
// turn off output
#define PMTK_SET_NMEA_OUTPUT_OFF "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"

#define PMTK_Q_RELEASE "$PMTK605*31"

void setup() {
  Serial.begin(115200); 
  //while (!Serial); // wait for Serial port to be ready
  mySerial.begin(9600);
  delay(2000);

  #ifdef USE_HARDWARE_SERIAL
  Serial.println("Using hardware serial");
  #else
   Serial.print("Using serial2 Rx=");
   Serial.print(Serial2.getRX());
   Serial.print(", Tx=");
   Serial.println(Serial2.getTX());
  #endif
  
  Serial.println("Get version!");
  mySerial.println(PMTK_Q_RELEASE);
  
  // you can send various commands to get it started
  //mySerial.println(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  mySerial.println(PMTK_SET_NMEA_OUTPUT_ALLDATA);

  mySerial.println(PMTK_SET_NMEA_UPDATE_1HZ);
 }


void loop() {
  if (Serial.available()) {
   char c = Serial.read();
   Serial.write(c);
   mySerial.write(c);
  }
  if (mySerial.available()) {
    char c = mySerial.read();
    Serial.write(c);
  }
}
