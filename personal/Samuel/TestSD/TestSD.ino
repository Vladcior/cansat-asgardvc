#include "SPI.h"
#include "SdFat.h"

SdFat sd;

const uint8_t chipSelect = SS;

void setup() {

  Serial.begin(9600);
  if (!sd.begin(chipSelect, SD_SCK_MHZ(50))) {
    sd.initErrorHalt();
  }

  sd.ls("/", 4);
  sd.ls("/", LS_DATE);
  sd.ls("/", LS_SIZE);

}

void loop() {
  // put your main code here, to run repeatedly:

}
