/*
   Logger.cpp
*/

#include <limits.h>
#include "Logger.h"

//#define USE_ASSERTIONS
//#define USE_TIMER

#define DEBUG
#include "DebugCSPU.h"
#include "Timer.h"

#define DBG_INIT 0
#define DBG_BUILD_FILE_NAME 0
#define DBG_FREE_SPACE 0
#define DBG_DIAGNOSTIC 1

Logger::Logger() {
  logFileName.reserve(13); // To avoid heap fragmentation.
  logFileName = "noname.txt";
}

signed char Logger::init( const char * fourCharPrefix,
                   const String& msg,
                   const unsigned int requiredFreeMegs) {
  DPRINTSLN(DBG_INIT, "Initializing Logger...");

  //DBG_TIMER("Logger::init");

  if (!initStorage()) return -1;
  DPRINTSLN(DBG_INIT, "Storage Init OK");

  if (requiredFreeMegs > 0) {
	  unsigned long freeMB = freeSpaceInMBytes();
	  DPRINTS(DBG_INIT, "FreeMB: Got ");
	  DPRINT(DBG_INIT, freeMB);
	  DPRINTS(DBG_INIT, ", required ");
	  DPRINTLN(DBG_INIT, requiredFreeMegs);

	  if (requiredFreeMegs > freeMB) {
		  DPRINTSLN(DBG_INIT, "Not enough space.");
		  return 1;
	  }
	  DPRINTSLN(DBG_INIT, "Required space OK");
  }
  else {
	  DPRINTSLN(DBG_INIT, "Required space not checked");
  }
  
  unsigned int counter = 1;
  do {
    buildFileName(fourCharPrefix, counter);
    counter++;
  } while (fileExists(logFileName.c_str()));

  DPRINTS(DBG_INIT, "Name of the file: ");
  DPRINTLN(DBG_INIT, logFileName); // use DPRINT and not DPRINTS to avoid using the "F" macro on a string object

  if (msg.length() > 0) {
    DPRINTS(DBG_INIT, "writing...");
    log(msg);
    DPRINTSLN(DBG_INIT, "written");
    DPRINTLN(DBG_INIT, msg);
  }
  DPRINTSLN(DBG_INIT, "logger initialized.");
  return 0;
}

bool Logger::doIdle() {
  bool result = false;
  if (timeSinceMaintenance > maintenancePeriod) {
    timeSinceMaintenance = 0;
    result = performMaintenance();
  }
  return result;
}

void Logger::buildFileName(const char* fourCharPrefix, const byte number) {

  logFileName = fourCharPrefix;
  DASSERT(logFileName.length() == 4);

  char numStr[5];
  sprintf(numStr, "%04d", number);

  logFileName += numStr;
  logFileName += ".txt";

  DPRINTS(DBG_BUILD_FILE_NAME, "Name of the file: ");
  DPRINTLN(DBG_BUILD_FILE_NAME, logFileName);
    // Do not used DPRINTSLN above to avoid wrapping the String in macro F() 
}

unsigned long Logger::freeSpaceInMBytes() {
     return (unsigned long) getFreeSpaceInMBytes();
};
    
unsigned long Logger::freeSpaceInBytes()  {
  const unsigned long overFlowLimit = ULONG_MAX / 1024;
  unsigned long  freeBytes = freeSpaceInMBytes();

  if (freeBytes >= overFlowLimit) {
    freeBytes = UINT_MAX;
  } else freeBytes *= 1024;

  DPRINTS(DBG_FREE_SPACE, "Free space bytes: ");
  DPRINTLN(DBG_FREE_SPACE, freeBytes);

  return freeBytes;
}
