/*
   Source file 1 for this library  
*/
//------------- Debugging control: never place in header file! -------
#define DEBUG              // define to enable debugging for this library
#include "DebugCSPU.h"   

#define DBG_TEST_LIB1 1
//--------------------------------------------------------------------

#include "test_lib1.h"

int dummyLibFunctionFromSrc1 () {
  DPRINTSLN(DBG_TEST_LIB1, "Library called (src1): blinking 3 times...");
  for (int i = 0 ; i < 3; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(durationOn);
    digitalWrite(LED_BUILTIN, LOW);
    delay(durationOn);
  }
  delay(500);
  return 0;
}
