/*
   Software to interact with the ADT7420 sensor on Diligent Pmod TMP2 break-out board.
   Ref: https://create.arduino.cc/projecthub/digilent/using-the-pmod-tmp2-with-arduino-uno-c172f2
        https://www.hackster.io/digilent/using-the-pmod-tmp2-with-arduino-uno-c172f2
*/

#include "IsatisConfig.h"
#include "ADT7420.h"
#include "Wire.h"


float ADT7420::readTemperature(const byte ADT7420_Address)
{
  byte MSB, LSB;
  float temperature=-1000.0;
  int val;

  Wire.beginTransmission(ADT7420_Address); // Launch of the measure
  byte result = Wire.endTransmission();
  if (result != 0)
  {
    DPRINTS(DBG_DIAGNOSTIC, "Error in transmission to ADT4720: ");
    DPRINTLN(DBG_DIAGNOSTIC, result);
  }
  delay(10);
  result = Wire.requestFrom(ADT7420_Address, 2); // Recovery of the two bytes MSB and LSB
  if (result != 2)
  {
    DPRINTS(DBG_DIAGNOSTIC, "Error: expected 2 bytes from ADT4720, got ");
    DPRINTLN(DBG_DIAGNOSTIC, result);
  }
  if (Wire.available() >= 2)  // Original was <=. Why ??
  {
    MSB = Wire.read();
    LSB = Wire.read();

    val = (MSB << 8) | LSB ;
    if (((val >> 15) & 1) == 0) // If the temperature is positive
    {
      temperature = val / 128.0;
    }
    else // If the temperature is negative
    {
      temperature = (val - 65535) / 128.0;
    }
  }
  else
  {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error: 2 bytes from ADT4720 not available");
  }
  return temperature;
}

// Initialization of Pmod TMP2 module
// Precondition: Wire library is initialized.
bool ADT7420::init(const byte ADT7420_Address)
{
  // Configuring the ADT7420 in 16 bit mode
  Wire.beginTransmission(ADT7420_Address);
  Wire.write(0x03);
  Wire.write(0x80);
  byte result = Wire.endTransmission();
  if (result != 0)
  {
    DPRINTS(DBG_DIAGNOSTIC, "Error in transmission to ADT4720: ");
    DPRINTLN(DBG_DIAGNOSTIC, result);
    return false;
  }
  else return true;
}
