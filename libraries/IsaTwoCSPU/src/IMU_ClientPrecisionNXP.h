/*
 * IMU_ClientPrecisionNXP.h
 */
 
#pragma once

#include "IMU_Client.h"
#include "Arduino.h"
#include "Wire.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_FXOS8700.h"
#include "Adafruit_FXAS21002C.h"
#include "IsaTwoRecord.h"

/** A class to query the LSM9DS0 Inertial Management Unit and 
 *  store the results into an IsaTwoRecord for broadcasting.
 */
class IMU_ClientPrecisionNXP : public IMU_Client
{
  public:
	static constexpr byte defaultNumSamplesPerRead=4;
    /** Constructor. Assumes Adafruit's Precision NXP 9-DOF is connected to the SDA and SCL pins
     */
	IMU_ClientPrecisionNXP(byte nSamplesPerRead=defaultNumSamplesPerRead);
    /** Initialize driver. Call before using readData()
     *  @return true if initialization ok.
     *  @pre Serial and Wire must be initialized
     */
    virtual bool begin();
  protected:
    /** Make a single reading of all 9 sensors, and store magnetic field in µT, gyro
     *  in raw sensor steps and rps, acceleration in raw sensor steps and m/s^2 to
     *  the current values in the record.
     *  NB: the record timestamp is NOT modified.
     *  @param record The record to update with the data.
     */
    virtual void readOneData(IsaTwoRecord& record);
    //void lsm(byte SDA, byte SCL);
  private:
    Adafruit_FXOS8700 accelmag;
    Adafruit_FXAS21002C gyro;
};
