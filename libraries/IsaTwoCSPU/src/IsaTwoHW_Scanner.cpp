/*
 * IsaTwoHW_Scanner.cpp
 */

#include "IsaTwoConfig.h"
#include "IsaTwoHW_Scanner.h"
#include "SdFat.h"

IsaTwoHW_Scanner::IsaTwoHW_Scanner(const byte unusedAnalogInPin) :
	HardwareScanner(unusedAnalogInPin)
#ifdef RF_ACTIVATE_API_MODE
    ,xbClientAvailable(false),
	xbClient(GroundXBeeAddressSH,GroundXBeeAddressSL)
#endif
{
	flags.SD_CardReaderAvailable = false;
	flags.BMP_SensorAvailable = false;
	flags.IMU_SensorAvailable = false;
}

void IsaTwoHW_Scanner::IsaTwoInit() {
  DPRINTSLN(DBG_INIT, "IsaTwoHW_Scanner::IsaTwoInit");
  analogWrite(CO2_AnalogWakePinNbr,0);
     // pull wake pin down to activate the CO2 sensor before scanning the bus
  HardwareScanner::init(I2C_lowestAddress, I2C_highestAddress, RF_SerialPortNumber, 400);
  analogWrite(CO2_AnalogWakePinNbr,255);
     // push wake pin up to deactivate the CO2 sensor  // Analog pins do not require any call to pinMode!
  pinMode(ImagerCtrl_MasterDigitalPinNbr, OUTPUT);
  digitalWrite(ImagerCtrl_MasterDigitalPinNbr,HIGH); // by default: do not start imager.
  // Do not configure USB debug pin: it is in use already.

  flags.BMP_SensorAvailable = isI2C_SlaveUpAndRunning( I2C_BMP_SensorAddress);
  flags.IMU_SensorAvailable = isI2C_SlaveUpAndRunning( I2C_IMU_SensorAddress);

  HardwareSerial* RF = getRF_SerialObject();
  if (RF) {
    RF->begin(RF_SerialBaudRate);
    while (!RF) ;
    DPRINTS(DBG_INIT, "RF Serial init ok at ");
    DPRINT(DBG_INIT, RF_SerialBaudRate );
    DPRINTSLN(DBG_INIT, " bauds");
#ifdef RF_ACTIVATE_API_MODE
    xbClient.begin(*RF); // Cannot fail
    xbClientAvailable=true;
#endif
  }
  else DPRINTSLN(DBG_DIAGNOSTIC, "*** No RF Serial port");

  if (!isSerialPortAvailable(GPS_SerialPortNumber)) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "*** No GPS Serial port");
  }
  else {
	  DPRINTSLN(DBG_INIT, "GPS Serial available");
  }

}

void IsaTwoHW_Scanner::checkAllSPI_Devices() {
  DPRINTSLN(DBG_CHECK_SPI_DEVICES, "IsaTwoHW_Scanner::checkAllSPI_Devices");
  // Check for SD Card reader
  SdFat SD;
  pinMode(MasterSD_CardChipSelect, OUTPUT);
  if (SD.begin(MasterSD_CardChipSelect)) {
    flags.SD_CardReaderAvailable = true;
  } else {
    DPRINTSLN(DBG_CHECK_SPI_DEVICES, "Card failed, or not present");
  }
  // do nothing more
}

byte IsaTwoHW_Scanner::getLED_PinNbr(LED_Type type) {
  byte result;
  switch (type) {
    case Init:
      result = pin_InitializationLED;
      break;
    case Storage:
      result =  pin_StorageLED;
      break;
    case Transmission:
      result =  pin_TransmissionLED;
      break;
    case Acquisition:
      result =  pin_AcquisitionLED;
      break;
    case Heartbeat:
      result =  pin_HeartbeatLED;
      break;
    case Campaign:
      result =  pin_CampaignLED;
      break;
    case UsingEEPROM:
      result =  pin_UsingEEPROM_LED;
      break;
    default:
      DASSERT(false);
      result =  0;
  }
  return result;
}

void IsaTwoHW_Scanner::printSPI_Diagnostic(Stream& stream) const {
  if (flags.SD_CardReaderAvailable) {
    stream.print(F("SPI bus: SD Card ok. CS="));
  }
  else {
    stream.print(F("*** SPI bus: no SD Card detected. *** CS="));
  }
  stream.println(MasterSD_CardChipSelect);
  return;
}


void IsaTwoHW_Scanner::printI2C_Diagnostic(Stream& stream) const {
  HardwareScanner::printI2C_Diagnostic(stream);
  delay(200);
  if (flags.BMP_SensorAvailable) {
    stream << F("Assuming I2C slave at ") << I2C_BMP_SensorAddress << F(" is the BMP sensor") << ENDL;
  }
  else
  {
    stream << F("*** Missing BMP sensor at I2C address ") << I2C_BMP_SensorAddress << ENDL;
  }
  if (!flags.IMU_SensorAvailable)
  {
	  stream << F("*** Missing IMU at I2C address ") << I2C_IMU_SensorAddress << ENDL;
  }
  if (isI2C_SlaveUpAndRunning(I2C_LSM9DS0_Address))
  {
	  stream << F("Assuming I2C slave at ") << I2C_LSM9DS0_Address << " and " << I2C_IMU_SensorSecondaryAddress << F(" is the LSM9DS0 IMU ");
	  stream << (I2C_IMU_SensorAddress == I2C_LSM9DS0_Address ? F("(Used)") : F("(Unused)"))<< ENDL;
  }
  if (isI2C_SlaveUpAndRunning(I2C_PrecisionNXP_Address))
  {
	  stream << F("Assuming I2C slave at ") << I2C_PrecisionNXP_Address << " and " << I2C_IMU_SensorSecondaryAddress << F(" is the Precision NXP IMU ");
	  stream << (I2C_IMU_SensorAddress == I2C_PrecisionNXP_Address ? F("(Used)") : F("(Unused)"))<< ENDL;
  }
  if (isI2C_SlaveUpAndRunning(I2C_Camera_SensorAddress))
  {
	  stream << F("Assuming I2C slave at ") << I2C_Camera_SensorAddress << F(" is the OV2640.") << ENDL;
  }
  if (isI2C_SlaveUpAndRunning(I2C_CO2_SensorAddress))
  {
	  stream << F("Assuming I2C slave at ") << I2C_CO2_SensorAddress << F(" is the CCS811 CO2 sensor.") << ENDL;
  }
}

#ifdef IGNORE_EEPROMS
byte IsaTwoHW_Scanner::getNumExternalEEPROM() const {
#ifndef NO_EEPROMS_ON_BOARD
  Serial << F("*** EEPROMS ignored for test ***") << ENDL;
#endif
  return 0;
}
#endif

void IsaTwoHW_Scanner::printFullDiagnostic(Stream& stream) const {
	HardwareScanner::printFullDiagnostic(stream);
	delay(300); // Avoid overflowing the stream buffer, in case
				// it is slow (e.g. a radio transmitter)...
	if (isSerialPortAvailable(RF_SerialPortNumber))  {
		stream << F("RF  assumed on Serial") << RF_SerialPortNumber
				<< F(" (") << RF_SerialBaudRate << F(" bauds)") << ENDL;
		if (RF_SerialPortNumber == 2) {
			stream << F("    TX=10, RX = 11") << ENDL;
		}
#ifdef RF_ACTIVATE_API_MODE
		if (xbClientAvailable) {
			stream << F("Using RF API mode. XBee client initialized.");
		} else {
			stream << F("*** RF API mode requested, but XBee client not initialized.");
		}
		stream << F("Using XBee set '") << RF_XBEE_MODULES_SET << "'" << ENDL;
		stream << F(" Receiver (ground) addr: 0x");
		stream.print(GroundXBeeAddressSH,HEX);
		stream << "-0x";
		stream.println(GroundXBeeAddressSL, HEX);
#else
		stream << F("Using RF TRANSPARENT mode");
#endif
	}

	else stream << F("No RF serial port (") << RF_SerialPortNumber << ")" << ENDL;
	if(isSerialPortAvailable(GPS_SerialPortNumber)) {
		stream << F("GPS assumed on Serial") << GPS_SerialPortNumber << ENDL;
	}
	else stream << F("No GPS serial port (") << GPS_SerialPortNumber << ")" << ENDL;
	delay(300); // Avoid overflowing the stream buffer, in case
				// it is slow (e.g. a radio transmitter...
	stream << F("Thermistor input on analog pin ") << Thermistor_AnalogInPinNbr << ENDL;
	stream << F("CO2 sensor wake on analog pin ") << CO2_AnalogWakePinNbr << ENDL;
	stream << F("Imager control on digital pin ") << ImagerCtrl_MasterDigitalPinNbr << ENDL;
	stream << F("Wait for USB Serial init: pull down digital pin ") << DbgCtrl_MasterDigitalPinNbr << ENDL;
}




